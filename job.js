'use strict'

/**
 * Job class constructor
 * @class Job
 * @constructor constructor for job class
 * @param {Object} gitlab - The gitlab repository
 * @param {Object} base - The path to the projects
 * @param {Object} obj - Used in object assign
 */
class Job {
  constructor (gitlab, base, obj) {
    Object.assign(this, obj)
    this.gitlab = gitlab
    this.base = `${base}/jobs/${this.id}`
    this.started_at = new Date(this.started_at)
    this.finished_at = new Date(this.finished_at)
  }

  /** Gets the duration of the job */
  get duration () {
    return Math.abs(this.finished_at - this.started_at)
  }
}

module.exports = Job
