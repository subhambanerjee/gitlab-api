'use strict'

const Item = require('./item.js')

class Group extends Item {
  constructor (gitlab, config, obj) {
    super(gitlab, config, obj)
    this.base = `${Group.base}/${this.id}`
  }

/** Gets all groups */
  static all (gitlab, config) {
    return gitlab.get(`${Group.base}?all_available=true&per_page=${config.per_page}`).then(arr => arr.map(obj => new Group(gitlab, config, obj)))
  }
}
Group.base = '/groups'
module.exports = Group
