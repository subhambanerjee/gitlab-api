'use strict'

/**
 * Variable class constructor
 * @constructor constructor for variable class
 * @param {Object} gitlab - The gitlab repository
 * @param {String} key - Key of the variable concerned
 * @param {String} base - Location of the project
 */
class Variable {
  constructor (key, base, gitlab) {
    this.gitlab = gitlab
    this.key = key
    this.base = `${base}/${Variable.base}/${this.key}`
    this.parent = `${base}/${Variable.base}`
  }
/** Calls get from gitlab class */
  get () {
    return this.gitlab.get(this.base)
  }
/** Calls put to update var value */
  update (value) {
    return this.gitlab.put(this.base, this.gitlab.object2params({value}))
  }

  /** Calls make to create var */
  create (value) {
    return this.gitlab.post(this.parent, this.gitlab.object2params({key: this.key, value}))
  }

  /** Calls del to delete var */

  delete () {
    return this.gitlab.del(this.base)
  }
}

Variable.base = '/variables'
module.exports = Variable
