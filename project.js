'use strict'

const Job = require('./job.js')
const Item = require('./item.js')

class Project extends Item {
  constructor (gitlab, config, obj) {
    super(gitlab, config, obj)
    this.base = `${Project.base}/${this.id}`
  }

  /** Gets all projects */
  static all (gitlab, config) {
    return gitlab.get(`${Project.base}?per_page=${config.per_page}`).then(arr => arr.map(obj => new Project(gitlab, config, obj)))
  }

 /** Getter for all jobs */
  get jobs () {
    return this.gitlab.get(`${this.base}/jobs?per_page=${this.max}`).then(arr => arr.map(obj => new Job(this.gitlab, this.base, obj)))
  }

  /** Returns file */
  file (path, ref) {
    return this.gitlab.get(`${this.base}/repository/files/${encodeURIComponent(path)}?ref=${ref}`)
       .then(fileObj => fileObj.content ? Buffer.from(fileObj.content, fileObj.encoding) : undefined)
       .catch(console.log('file not found in:' + `${this.base}`))
  }

  /** Getter all users */
  get users () {
    return this.gitlab.get(`${this.base}/users?per_page=${this.max}`)
  }
}

Project.base = '/projects'
module.exports = Project
