'use strict'
/**
 * This is the project class that requires the module {@link module : gitlab-cli/node_modules/parse-link-header},
 * {@link module : gitlab-cli/node_modules/node-fetch-npm} and
 * {@link module : url}
 * @class Gitlab
 * @requires module:gitlab-cli/node_modules/parse-link-header
 * @requires module:gitlab-cli/node_modules/node-fetch-npm

 */
const URLSearchParams = require('url').URLSearchParams
const fetch = require('node-fetch-npm')
const parse = require('parse-link-header')
/**
 * Gitlab class constructor
 * @constructor constructor for gitlab class
 * @param {String} url
 * @param {String} token
 */
module.exports = class {
  constructor (url, token) {
    this.options = {}
    this.options.headers = {
      'PRIVATE-TOKEN': token
    }
    this.url = `${url}/api/v4`
  }
  /**
   * Get method implementation
   * @param {string} path path to perform operation
   */
  get (path) {
    return this.doFetch(path)
  }

  /**
   * Put method implementation
   * @param {string} path path to perform operation
   * @param {*} body consists the value to be updated
   */
  put (path, body) {
    return this.doFetch(path, {method: 'PUT', body})
  }
  /**
   * Del method implementation
   * @param {string} path path to perform operation
   */
  del (path) {
    return this.doFetch(path, {method: 'DELETE'})
  }
  /**
   * Creating a new key/value pair
   * @param {string} path path to perform operation
   * @param {*} body consists the key and value to be used
   */
  post (path, body) {
    return this.doFetch(path, {method: 'POST', body})
  }
  /**
   * Pagination implemented
   * @param {string} path path to perform operation
   * @param {*} options consists the body and the method such as PUT, DELETE, POST
   */
  async doFetch (path, options) {
    let retVal = []
    let res
    let url = `${this.url}${path}`
    options = Object.assign(options || {}, this.options)
    if (options.body instanceof URLSearchParams) {
      options.headers['Content-Type'] = 'multipart/form-data'
    }

    while (url) {
      res = await fetch(url, options)
      if (res.status > 499) {
        throw new Error(`${res.url}: ${res.status} ${res.statusText}`)
      }

      url = undefined
      try {
        url = parse(res.headers.get('Link')).next.url
      } catch (e) {
        // header missing or last page
      }
      res = await res.json()
      if (Array.isArray(res)) {
        retVal.push(...res)
      } else {
        retVal = res
      }
    }
    return retVal
  }

  /**
   * URLSearchParams method used to append the key value pair and return a string(no need of calling toString)
   * @param {object} obj key/value pair passed as object
   */
  object2params (obj) {
    const searchparams = new URLSearchParams()
    for (const key in obj) {
      searchparams.append(key, obj[key])
    }
    return searchparams
  }
}
