'use strict'

const Variable = require('./variable.js')

class Item {
  constructor (gitlab, config, obj) {
    Object.assign(this, obj)
    this.gitlab = gitlab
    this.config = config
    this.max = config.per_page // pagination - maximum data shown per page - mentioned in config file
  }

/** Getter for all variables */
  get variables () {
    return this.gitlab.get(`${this.base}/variables?per_page=${this.max}`)
  }

/** Gets a var corresponding to key */
  variable (key) {
    return new Variable(key, this.base, this.gitlab)
  }
}
module.exports = Item
