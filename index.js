const Gitlab = require('./gitlab.js')
const Group = require('./group.js')
const Project = require('./project.js')
const Job = require('./job.js')
const Variable = require('./variable.js')
module.exports = {Gitlab, Group, Project,Job, Variable}